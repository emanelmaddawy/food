/* global src */

$(document).ready(function () {


new WOW().init();

$('.owl-carousel#sync1').owlCarousel({
  items:1,
  loop:true,
  nav: true,
  navText: ['<i class="fa fa-angle-left"><i>', '<i class="fa fa-angle-right"><i>'],
  URLhashListener:true,
  autoplayHoverPause:true,
  startPosition: 'URLHash'
});

$('.owl-carousel#sync2').owlCarousel({
  items:4,
  loop:true,
  nav: true,
  navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
  URLhashListener:true,
  autoplayHoverPause:true,
  startPosition: 'URLHash',
  responsive:{
      0:{
          items:1,
          mouseDrag:false
      },
      600:{
          items:3,
          mouseDrag:true
      },
      1000:{
          items:4
      }
  }
});

$('.owl-carousel#sync3').owlCarousel({
  items:1,
  loop:true,
  nav: true,
  navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
  URLhashListener:true,
  autoplayHoverPause:true,
  startPosition: 'URLHash',
  responsive:{
      0:{
          items:1,
          mouseDrag:false
      },
      600:{
          items:2,
          mouseDrag:true,
          margin:10
      },
      1000:{
          items:1
      }
  }

});
});
